<?php get_header(); ?>
<div class="intro">
		<div class="container">
			<div class="row row-eq-height justify-content-md-center">
				<?php
        // The Loop
        if (have_posts()):
            while (have_posts()): the_post();
                ?>
                    <!-- Intro Content -->
				    <div class="col-lg-8 col">
					<div class="intro_content">
						<h2><?php the_title()?></h2>
						<p><?php the_content()?></p>
					</div>
				</div>
                <?php endwhile;
        endif;

        wp_reset_query();
        ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>