<?php
/*
	Template Name: Template liste des offres
*/

get_header(); ?>
<!-- Listings -->

	<div class="listings">
		<div class="container">
			<div class="row">
				<div class="col">
					<!-- Sorting -->
					<div class="sorting d-flex flex-md-row flex-column align-items-start justify-content-start">
						<!-- Tags -->
						<div class="listing_tags">
							<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
								<li><a href="#">house</a></li>
								<li><a href="#">apartment</a></li>
								<li><a href="#">garage</a></li>
								<li><a href="#">swimming pool</a></li>
								<li><a href="#">patio</a></li>
								<li><a href="#">heated floors</a></li>
								<li><a href="#">garden</a></li>
							</ul>
						</div>
					</div>

					<!-- Listings Container -->
					<div class="listings_container">
                    <?php 
                    // The Query
                        $args = array(
                            'posts_per_page' => '-1',
                            'post_type' => 'offre',
                            'meta_key' => 'date',
                            'orderby' => 'meta_value',
                        );
                        query_posts($args);
                        // The Loop
                        if (have_posts()):
                            while (have_posts()): the_post();
                                ?>

                               <!-- Listing -->
						<div class="listing_box house sale">
							<div class="listing">
								<div class="listing_image">
									<div class="listing_image_container">
                                    <?php 
                                    if (has_post_thumbnail()) {
                                        $thumbnail = get_the_post_thumbnail_url();
                                        echo('<img src="'.$thumbnail.'" alt="">');
                                    } else {
                                        $thumbnail = get_template_directory_uri() . '/images/listing_1.jpg';
                                        echo('<img src="'.$thumbnail.'" alt="" style="filter:grayscale(0.9) blur(5px)">');
                                    }
                                    ?>
										<img src="images/listing_1.jpg" alt="">
									</div>
									<div class="tags d-flex flex-row align-items-start justify-content-start flex-wrap">
										<div class="tag tag_house"><a href="listings.html">house</a></div>
										<div class="tag tag_sale"><a href="listings.html">for sale</a></div>
									</div>
									<div class="tag_price listing_price">$ <?php the_field('prix')?></div>
								</div>
								<div class="listing_content">
									<div class="prop_location listing_location d-flex flex-row align-items-start justify-content-start">
										<img src="<?=get_template_directory_uri()?>/images/icon_1.png" alt="">
										<a href="<?php the_permalink()?>"><?php the_field('adresse')?></a>
									</div>
									<div class="listing_info">
										<ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
											<li class="property_area d-flex flex-row align-items-center justify-content-start">
												<img src="<?=get_template_directory_uri()?>/images/icon_2.png" alt="">
												<span><?php the_field('superficie')?> sq ft</span>
											</li>
											<li class="d-flex flex-row align-items-center justify-content-start">
												<img src="<?=get_template_directory_uri()?>/images/icon_3.png" alt="">
												<span><?php the_field('nb_salle_de_bain')?></span>
											</li>
											<li class="d-flex flex-row align-items-center justify-content-start">
												<img src="<?=get_template_directory_uri()?>/images/icon_4.png" alt="">
												<span><?php the_field('truc_petite_maison')?></span>
											</li>
											<li class="d-flex flex-row align-items-center justify-content-start">
												<img src="<?=get_template_directory_uri()?>/images/icon_5.png" alt="">
												<span><?php the_field('garage')?></span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

                        <?php endwhile;
                        endif;

                        wp_reset_query();
                    ?>
						

					</div>
				
				</div>
			</div>
		</div>
	</div>


<?php get_footer(); ?>