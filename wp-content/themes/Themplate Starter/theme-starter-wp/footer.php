	<!-- Footer -->
	
	<footer class="footer">
		<div class="footer_content">
			<div class="container">
				<div class="row">
					
					<!-- Footer Column -->
					<div class="col-xl-3 col-lg-6 footer_col">
						<div class="footer_about">
							<div class="footer_logo"><a href="#">my<span>home</span></a></div>
							<div class="footer_text">
								<p><?php the_field("text",16)?></p>
							</div>
						</div>
					</div>
					
					<!-- Footer Column -->
					<div class="col-xl-3 col-lg-6 footer_col">
						<div class="footer_column">
							<div class="footer_title">Information</div>
							<div class="footer_info">
								<ul>
									<!-- Phone -->
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div><img src="<?=get_template_directory_uri()?>/images/phone-call.svg" alt=""></div>
										<span>+<?php the_field("tel",16)?></span>
									</li>
									<!-- Address -->
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div><img src="<?=get_template_directory_uri()?>/images/placeholder.svg" alt=""></div>
										<span><?php the_field("adresee",16)?></span>
									</li>
									<!-- Email -->
									<li class="d-flex flex-row align-items-center justify-content-start">
										<div><img src="<?=get_template_directory_uri()?>/images/envelope.svg" alt=""></div>
										<span><?php the_field("mail",16)?></span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					
					<!-- Footer Column -->
					<div class="col-xl-3 col-lg-6 footer_col">
						<div class="footer_links">
							<div class="footer_title">Usefull Links</div>
							<ul>
								<li><a href="#">Testimonials</a></li>
								<li><a href="#">Listings</a></li>
								<li><a href="#">Featured Properties</a></li>
								<li><a href="#">Contact Agents</a></li>
								<li><a href="#">About us</a></li>
							</ul>
						</div>
					</div>
					
					<!-- Footer Column -->
					<div class="col-xl-3 col-lg-6 footer_col">
						<div class="social">
							<ul class="d-flex flex-row align-items-center justify-content-start">
                            <?php
                            if(!empty(get_field('pinterest',16))){
                                echo('<li><a href="'.the_field("pinterest",16).'"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>');
                            }
                            if(!empty(get_field('fb',16))){
                                echo('<li><a href="'.the_field("fb",16).'"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>');
                            } 
                            if(!empty(get_field('twitter',16))){
                                echo('<li><a href="'.the_field("twitter",16).'"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>');
                            } 
                            if(!empty(get_field('insta',16))){
                                echo('<li><a href="'.the_field("insta",16).'"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>');
                            } 
                            if(!empty(get_field('behance',16))){
                                echo('<li><a href="'.the_field("behance",16).'"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>');
                            } 
                            if(!empty(get_field('dribbble',16))){
                                echo('<li><a href="'.the_field("dribbble",16).'"><i class="fa fa-behance" aria-hidden="true"></i></a></li>');
                            } 
                            ?>
							</ul>
						</div>
						<div class="footer_submit"><a href="<?php the_permalink(16)?>">Contact us</a></div>
					</div>
				
				</div>
			</div>
		</div>
		<div class="footer_bar">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="footer_bar_content d-flex flex-md-row flex-column align-items-md-center align-items-start justify-content-start">
							<div class="copyright order-md-1 order-2">
								Copyright &copy;2019 All rights reserved | Licence Pro Multimédia
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</div>

<script src="<?=get_template_directory_uri()?>/js/jquery-3.3.1.min.js"></script>
<script src="<?=get_template_directory_uri()?>/styles/bootstrap-4.1.2/popper.js"></script>
<script src="<?=get_template_directory_uri()?>/styles/bootstrap-4.1.2/bootstrap.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/OwlCarousel2-2.3.4/owl.carousel.js"></script>
<script src="<?=get_template_directory_uri()?>/js/easing/easing.js"></script>
<script src="<?=get_template_directory_uri()?>/js/scrollmagic/ScrollMagic.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/progressbar/progressbar.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/parallax-js-master/parallax.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/script.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>

		<?php wp_footer(); ?>
  
	</body>
</html>
