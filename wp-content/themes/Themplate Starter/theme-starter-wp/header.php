<!doctype html>
<html <?php language_attributes();?> <?php body_class();?> >
<head>
    <meta charset="<?php bloginfo('charset');?>">

    <title><?php wp_title('');?></title>

    <link href="//www.google-analytics.com" rel="dns-prefetch">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

    <?php wp_head();?>

    <!-- LIEN CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/bootstrap-4.1.2/bootstrap.min.css">
    <link href="<?php echo get_template_directory_uri(); ?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/OwlCarousel2-2.3.4/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/OwlCarousel2-2.3.4/owl.theme.default.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/OwlCarousel2-2.3.4/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/main.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/responsive.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/styles/custom.css">

</head>
<body class="no-js">


<div class="super_container">
	<div class="super_overlay"></div>

	<!-- Header -->

	<header class="header">

		<!-- Header Content -->
		<div class="header_content d-flex flex-row align-items-center justify-content-start">
			<div class="logo"><a href="<?=get_site_url()?>">my<span>home</span></a></div>
			<nav class="main_nav">
            <?php wp_nav_menu(
    array(
        'theme-location' => 'header-menu',
        'menu_class' => 'd-flex flex-row align-items-start justify-content-start',
    )
);?>
			</nav>
			<div class="hamburger ml-auto"><i class="fa fa-bars" aria-hidden="true"></i></div>
		</div>

	</header>

    <?php
if (!is_front_page()) {
    if (has_post_thumbnail()) {
        $thumbnail = get_the_post_thumbnail_url();
    } else {
        $thumbnail = get_template_directory_uri() . '/images/about.jpg';
    }
    ?>	<!-- Home -->
        <div class="banner">
            <div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="<?=$thumbnail?>" data-speed="0.8"></div>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="banner_content text-center">
                            <div class="banner_title"><?php the_title();?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
}
?>