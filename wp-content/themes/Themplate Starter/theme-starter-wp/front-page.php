<?php get_header(); ?>

<!-- Home -->

	<div class="banner banner--home">
		
		<!-- Home Slider -->
		<div class="banner_slider_container">
			 <div class="owl-carousel owl-theme home_slider">
			 	<?php 
                    // The Query
                        $args = array(
                            'posts_per_page' => '3',
                            'post_type' => 'offre',
                            'meta_key' => 'date',
                            'orderby' => 'meta_value',
                        );
                        query_posts($args);
                        // The Loop
                        if (have_posts()):
                            while (have_posts()): the_post();
                                
                                    if (has_post_thumbnail()) {
                                        $thumbnail = get_the_post_thumbnail_url();
                                    } else {
                                        $thumbnail = get_template_directory_uri() . '/images/listing_1.jpg';
                                    }
                                    
                                ?>

                               <!-- Listing -->
                            <!-- Slide -->
                            <div class="slide">
                                <div class="background_image" style="background-image:url(<?=$thumbnail?>)"></div>
                                <div class="banner_container">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                <div class="banner_content">
                                                    <div class="banner_title"><h1><?php the_field('adresse')?></h1></div>
                                                    <div class="banner_price_tag">$ <?php the_field('prix')?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile;
                        endif;

                        wp_reset_query();
                    ?>
			 	

			 </div>
			 
		</div>
	</div>

	<!-- Featured -->

	<div class="featured">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<div class="section_subtitle">the best deals</div>
						<div class="section_title"><h1>Featured Properties</h1></div>
					</div>
				</div>
			</div>
			<div class="row featured_row">
				<?php 
                    // The Query
                        $args = array(
                            'posts_per_page' => '3',
                            'post_type' => 'offre',
                            'orderby' => 'rand',
                        );
                        query_posts($args);
                        // The Loop
                        if (have_posts()):
                            while (have_posts()): the_post();
                                
                                    if (has_post_thumbnail()) {
                                        $thumbnail = get_the_post_thumbnail_url();
                                    } else {
                                        $thumbnail = get_template_directory_uri() . '/images/listing_1.jpg';
                                    }
                                    
                                ?>

                            <!-- Featured Item -->
                            <div class="col-lg-4">
                                <div class="listing">
                                    <div class="listing_image">
                                        <div class="listing_image_container">
                                            <img src="<?=$thumbnail?>" alt="">
                                        </div>
                                        <div class="tags d-flex flex-row align-items-start justify-content-start flex-wrap">
                                            <div class="tag tag_house"><a href="listings.html">house</a></div>
                                            <div class="tag tag_sale"><a href="listings.html">for sale</a></div>
                                        </div>
                                        <div class="tag_price listing_price">$ <?php the_field('prix')?></div>
                                    </div>
                                    <div class="listing_content">
                                        <div class="prop_location listing_location d-flex flex-row align-items-start justify-content-start">
                                            <img src="<?=get_template_directory_uri()?>/images/icon_1.png" alt="">
                                            <a href="offer.html"><?php the_field('adresse')?></a>
                                        </div>
                                        <div class="listing_info">
                                            <ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
                                                <li class="property_area d-flex flex-row align-items-center justify-content-start">
                                                    <img src="<?=get_template_directory_uri()?>/images/icon_2.png" alt="">
                                                    <span><?php the_field('superficie')?> sq ft</span>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <img src="<?=get_template_directory_uri()?>/images/icon_3.png" alt="">
                                                    <span><?php the_field('nb_salle_de_bain')?></span>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <img src="<?=get_template_directory_uri()?>/images/icon_4.png" alt="">
                                                    <span><?php the_field('truc_petite_maison')?></span>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                <img src="<?=get_template_directory_uri()?>/images/icon_5.png" alt="">
                                                <span><?php the_field('garage')?></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile;
                        endif;

                        wp_reset_query();
                        ?>
				


			</div>
		</div>
	</div>

	<!-- Map Section -->

	<div class="map_section container_reset">
		<div class="container">
			<div class="row row-xl-eq-height">

				<!-- Map -->
				<div class="col-xl-7 order-xl-1 order-2">
					<div class="map">
						<div id="google_map" class="google_map">
							<div class="map_container">
								<div id="map"></div>
							</div>
						</div>
					</div>
				</div>

				<!-- Content -->
				<div class="col-xl-5 order-xl-2 order-1">
					<div class="map_section_content">
						<div class="map_overlay">
							<svg fill="#55407d" width="100%" height="100%" viewBox="0 0 100 100" preserveAspectRatio="none">
								<path d="M100,0 a-200,150 0 0 0 -100,100 h100 v-100 z" />
							</svg>
						</div>
						<div class="section_title_container">
							<div class="section_subtitle">the best deals</div>
							<div class="section_title"><h1>Choose a location</h1></div>
						</div>
						<div class="locations_list d-flex flex-column align-items-start justify-content-start">
                        <?php
                        for ($i=1; $i < 11; $i++) {
                                $loc=get_field('location_'.$i);
                                if(!empty($loc['ville'])){
                                    ?>
                                    <label class="location_contaner" data-lat="<?=$loc['lattitude']?>" data-lng="<?=$loc['longitude']?>"> 
                                        <input type="radio" name="location_radio">
                                        <span></span>
                                        <?=$loc['ville']?>
                                    </label>
                                    <?php
                                }
                            } ?>
							
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- Hot -->

	<div class="hot">
		<div class="container">
            <?php
            $post_object = get_field('tdh');

        if( $post_object ): 

        // override $post
        $post = $post_object;
        setup_postdata( $post ); 
        if (has_post_thumbnail()) {
            $thumbnail = get_the_post_thumbnail_url();
        } else {
            $thumbnail = get_template_directory_uri() . '/images/listing_1.jpg';
        }

    ?>
			<div class="row">
				<div class="col">
					<div class="section_title_container text-center">
						<div class="section_subtitle">the best deals</div>
						<div class="section_title"><h1>Today's Hot Deal</h1></div>
					</div>
				</div>
			</div>
			<div class="row hot_row row-eq-height">
				
				<!-- Hot Deal Image -->
				<div class="col-lg-6">
					<div class="hot_image">
						<div class="background_image" style="background-image:url(<?=$thumbnail?>)"></div>
						<div class="tags d-flex flex-row align-items-start justify-content-start flex-wrap">
							<div class="tag tag_house"><a href="listings.html">house</a></div>
							<div class="tag tag_sale"><a href="listings.html">for sale</a></div>
						</div>
					</div>
				</div>

				<!-- Hot Deal Content -->
				<div class="col-lg-6">
					<div class="hot_content">
						<div class="hot_deal">
							<div class="tag_price">$ <?php the_field('prix')?></div>
							<div class="hot_title"><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
							<div class="prop_location d-flex flex-row align-items-start justify-content-start">
								<img src="<?=get_template_directory_uri()?>/images/icon_1.png" alt="">
								<span><?php the_field('adresse')?></span>
							</div>
							<div class="prop_text">
								<p><?php the_excerpt();?></p>
							</div>
							<div class="prop_agent d-flex flex-row align-items-center justify-content-start">
								<div class="prop_agent_image"><img src="<?=get_template_directory_uri()?>/images/agent_1.jpg" alt=""></div>
								<div class="prop_agent_name"><a href="#">Maria Smith,</a> Agent</div>
							</div>
						</div>
						<div class="prop_info">
							<ul class="d-flex flex-row align-items-center justify-content-start flex-wrap">
								<li class="d-flex flex-row align-items-center justify-content-start">
									<img src="<?=get_template_directory_uri()?>/images/icon_2_large.png" alt="">
									<div>
										<div><?php the_field('superficie')?></div>
										<div>sq ft</div>
									</div>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<img src="<?=get_template_directory_uri()?>/images/icon_3_large.png" alt="">
									<div>
										<div><?php the_field('nb_salle_de_bain')?></div>
										<div>baths</div>
									</div>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<img src="<?=get_template_directory_uri()?>/images/icon_4_large.png" alt="">
									<div>
										<div><?php the_field('truc_petite_maison')?></div>
										<div>beds</div>
									</div>
								</li>
								<li class="d-flex flex-row align-items-center justify-content-start">
									<img src="<?=get_template_directory_uri()?>/images/icon_5_large.png" alt="">
									<div>
										<div><?php the_field('garage')?></div>
										<div>garages</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>
	</div>

	<!-- Testimonials -->

	<div class="testimonials container_reset">
		<div class="container">
			<div class="row row-eq-height">
				
				<!-- Testimonials Image -->
				<div class="col-xl-6">
					<div class="testimonials_image">
						<div class="background_image" style="background-image:url(<?=get_template_directory_uri()?>/images/testimonials.jpg)"></div>
						<div class="testimonials_image_overlay"></div>
					</div>
				</div>

                    
				<!-- Testimonials Content -->
				<div class="col-xl-6">
					<div class="testimonials_content">
						<div class="section_title_container">
							<div class="section_subtitle">the best deals</div>
							<div class="section_title"><h1>Clients testimonials</h1></div>
						</div>

						<!-- Testimonials Slider -->
						<div class="testimonials_slider_container">
							<div class="owl-carousel owl-theme test_slider">
								<?php
                                    for ($i=1; $i < 4; $i++) {
                                        $temoignage=get_field('temoignage_'.$i);
                                    ?>
                                    <div class="test_slide">
                                        <div class="test_quote">"<?=$temoignage['titre']?>"</div>
                                        <div class="test_text">
                                            <p><?=$temoignage['contenue']?></p>
                                        </div>
                                        <div class="test_author"><?=$temoignage['auteur']?>, Client</div>
                                    </div>
                                <?php
                                    }
                                ?>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
<?php get_footer(); ?>