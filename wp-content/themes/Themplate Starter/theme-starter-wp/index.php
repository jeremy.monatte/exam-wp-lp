<?php get_header(); ?>


<div class="blog">
    <div class="container">
        <div class="row">

            <!-- Blog Posts -->
            <div class="col-lg-9">
                <div class="blog_posts">

                    <?php
                    // The Query
                        $args = array(
                            'posts_per_page' => '3',
                            'post_type' => 'post',
                        );
                        query_posts($args);
                        // The Loop
                        if (have_posts()):
                            while (have_posts()): the_post();

                            if (has_post_thumbnail()) {
                                $thumbnail = get_the_post_thumbnail_url();
                            } else {
                                $thumbnail = get_template_directory_uri() . '/images/about.jpg';
                            }

                            ?>

                    <!-- Blog Post -->
                    <div class="blog_post">
                        <div class="blog_post_image">
                            <img src="<?=$thumbnail?>" alt="">
                            <div class="blog_post_date d-flex flex-column align-items-center justify-content-center">
                                <?php the_date('\<\d\i\v\>j\<\/\d\i\v\>\<\d\i\v\>M Y\<\/\d\i\v\>')?>
                            </div>
                        </div>
                        <div class="blog_post_content">
                            <div class="blog_post_title"><a href="#"><?php the_title()?></a></div>
                            <div class="blog_post_info">
                                <ul class="d-flex flex-row align-items-start justify-content-start flex-wrap">
                                    <li>by <?php the_author_posts_link()?></li>
                                    <li>
                                        <?php the_tags('<ul class="d-flex flex-row align-items-start justify-content-start"><li>','</li>
                                            <li>','</li>
                                        </ul>') ?>
                                    </li>
                                </ul>
                            </div>
                            <div class="blog_post_text">
                                <p><?php the_excerpt();?></p>
                            </div>
                        </div>
                    </div>
                     <?php endwhile;
                        endif;

                        wp_reset_query();
                    ?>

                </div>
                <div class="page_nav">
                    <ul class="d-flex flex-row align-items-start justify-content-start">
                        <li class="active"><a href="#">01.</a></li>
                        <li><a href="#">02.</a></li>
                        <li><a href="#">03.</a></li>
                    </ul>
                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-lg-3">
                <div class="sidebar">
                    <!-- Categories -->
                    <div class="categories">
                        <div class="sidebar_title"><h3>Categories</h3></div>
                        <div class="sidebar_list">
                            <ul>
                            <?php wp_list_categories(array('title_li'=>""))?>
                            </ul>
                        </div>
                    </div>

                    <!-- Tags -->
                    <div class="sidebar_tags">
                        <div class="sidebar_title"><h3>Tags</h3></div>
                        <?php

                        $before='<ul class="d-flex flex-row align-items-start justify-content-start flex-wrap"><li>';
                        $sep='</li><li>';
                        $after='</li></ul>';
                        echo get_the_tag_list($before, $sep, $after);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>