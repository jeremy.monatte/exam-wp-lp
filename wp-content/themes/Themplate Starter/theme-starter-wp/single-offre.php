<?php get_header();?>
	<div class="single_container">
		<div class="container">
			<div class="row">
				<div class="col">

					<!-- Image -->
					<div class="single_image"><img src="images/listing.jpg" alt=""></div>

					<!-- About -->
					<div class="about">
						<div class="row">
							<div class="col-lg-6">
								<div class="property_info">
									<div class="tag_price single_price">$ <?php the_field('prix');?></div>
									<div class="single_name"><h1><?php the_title();?></h1></div>
									<div class="single_location d-flex flex-row align-items-start justify-content-start">
										<img src="images/icon_1.png" alt="">
										<span><?php the_field('adresse');?></span>
									</div>
									<div class="single_list">
										<ul>
											<li>Property ID: <?php the_field('property_id');?></li>
                                            <li>Posted on: <?php
                                            $date=get_field('date');
                                             the_field('date');?></li>
											<li>Off plan</li>
										</ul>
									</div>
									<div class="prop_agent d-flex flex-row align-items-center justify-content-start">
										<div class="prop_agent_image"><img src="images/agent_1.jpg" alt=""></div>
										<div class="prop_agent_name"><a href="mailto:mariasmith@gmail.com">Maria Smith,</a> Agent</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="about_text">
									<p><?php the_content();?></p>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="single_features">
									<div class="single_title"><h3>Features</h3></div>
									<div class="row">
										<div class="col-lg-6">
											<ul>
												<li>2 car garages</li>
												<li>3 bedrooms</li>
												<li>heated floors</li>
												<li>contemporary architecture</li>
												<li>swimming pool</li>
												<li>exercise room</li>
												<li>formal entry</li>
											</ul>
										</div>
										<div class="col-lg-6">
											<ul>
												<li>patio</li>
												<li>close to stores</li>
												<li>ocean view</li>
												<li>spa</li>
												<li>sprinklers</li>
												<li>garden</li>
												<li>alley</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="single_video">
									<div class="single_title"><h3>Video</h3></div>
									<div class="video_container d-flex flex-column align-items-center justify-content-center">
										<img src="images/video.jpg" alt="">
										<div class="video_button"><a class="youtube" href="https://www.youtube.com/embed/C0DPdy98e4c?autoplay=1"><i class="fa fa-play" aria-hidden="true"></i></a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php get_footer();?>